
let arg = [];

function agregar(){
    let agrega = document.getElementById('inlineFormInputGroupUsername').value;
        arg.push(agrega);
        actualizaPantalla();
        document.getElementById('inlineFormInputGroupUsername').value="";
}

function removeItem (item) {
   arg.splice(item,1);
   actualizaPantalla();
}

function actualizaPantalla(){
    document.querySelector('#checkmaster').innerHTML ='';
    arg.forEach((dato, index) => {
        document.querySelector('#checkmaster').insertAdjacentHTML(
            'afterbegin',
            `<div  class="form-check">
                <input class="form-check-input" type="checkbox" id="inlineFormCheck">
                <label class="form-check-label" for="inlineFormCheck">
                    ${dato}  
                </label>
                <button id="elimina" type="submit" class="btn btn-warning" onclick="removeItem(${index})">X</button>
             </div>`
          );
    });
}

